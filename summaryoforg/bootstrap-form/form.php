<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Add Form - Summary of Organiztion</title>

    <!-- Bootstrap -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container">
    <h2>Form - Add Summary of Organization</h2>
    <form>
      <div class="form-group">
        <label for="name">Organization Name:</label>
        <input type="text" class="form-control" id="name" placeholder="Organization Name">

      </div>
      <div class="form-group">
        <label for="summary">Summary of Organization:</label>
        <textarea class="form-control" id="summary" placeholder="Type Summary here"></textarea>
      </div>

      <button type="submit" class="btn btn-info">Add</button>
      <button type="reset" class="btn btn-danger">Reset</button>
    </form>
  </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../assets/bootstrap/js//bootstrap.min.js"></script>
  </body>
</html>