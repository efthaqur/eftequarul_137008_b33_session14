<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Add Hobbies</title>

    <!-- Bootstrap -->
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container">
    <h2>Add Hobbies</h2>
    <form>
      <div class="form-group">
        <label for="name">User Name:</label>
        <input type="text" class="form-control" id="name" placeholder="User Name Here">
      </div>

      <div class="form-group">

        <label for=""><input type="checkbox" vale="Painting">Painting</label>
        <label for=""><input type="checkbox" value="Travelling">Traveling</label>
        <label for=""><input type="checkbox" value="Movies">Movies</label>
        <label for=""><input type="checkbox" value="Dancing">Dancing</label>
        <label for=""><input type="checkbox" value="Coding">Coding</label>
        <label for=""><input type="checkbox" value="Driving">Driving</label>
        <label for=""><input type="checkbox" value="Gardening">Gardening</label>
        <label for=""><input type="checkbox" value="Singing">Singing</label>
        <label for=""><input type="checkbox" value="Reading">Reading</label>
        <label for=""><input type="checkbox" value="Cooking">Cooking</label>




      </div>

      <button type="submit" class="btn btn-info">Submit</button>
      <button type="reset" class="btn btn-danger">Reset</button>
    </form>
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../assets/bootstrap/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../assets/bootstrap/js//bootstrap.min.js"></script>
  </body>
</html>